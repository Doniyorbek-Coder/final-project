var output = document.querySelector("#output");
var btn = document.querySelectorAll(".calc__btn");
let results = document.querySelector("#results");

for (item of btn) {
  item.addEventListener("click", (e) => {
    btntext = e.target.innerText;
    if (btntext == "%") {
      btntext = "/100";
    }
    if (btntext == "×") {
      btntext = "*";
    }
    if (btntext == "÷") {
      btntext = "/";
    }
    output.value += btntext;
  });
}

function result() {
  if (output.value === "") {
    results.value = "";
    output.value = "";
  } else {
    results.value = eval(output.value);
    output.value += "=";
    if (results.value.length > 9) {
      let res = results.value.toString();
      res = res.substring(-1, 10);
      results.value = res;
    }
  }
}

function cleared() {
  results.value = "";
  output.value = "";
}
